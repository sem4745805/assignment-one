#include <stdio.h>
#include <math.h>

void calculate(int a, int b, int c) {
	int discriminant = pow(b,2) - (4 * a * c);
	double root_1 = (-b - sqrt(discriminant))/ (2*a);
	double root_2 = (-b + sqrt(discriminant))/ (2*a);

	if (discriminant < 0){
		printf("\ncomplex\n");
	}
	else if  (discriminant == 0) {
		printf("\nThe roots are real and equal.\n");
		printf("The root is:\n X = %.2f\n",root_1);
	}
	else {
		printf("\nThe root are real.\n");
		printf("The roots are:\n X1 = %.2f\n X2 = %.2f\n",root_1, root_2);
	}
 }

int main(void) {
	int a,b,c;

	printf("Enter the coefficient (a): ");
	scanf("%d",&a);
	printf("Enter the coefficient (b): ");
	scanf("%d",&b);
	printf("Enter the coefficient (c): ");
	scanf("%d",&c);

	if (a == 0){
		printf("Invalid input.\n");
		return 1;
	}

	calculate(a,b,c);
	return 0;
}